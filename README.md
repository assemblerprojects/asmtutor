See https://asmtutor.com

* [Linux System Call Table & OPCODES](https://www.chromium.org/chromium-os/developer-library/reference/linux-constants/syscalls/)

* [Linux Syscalls](https://syscall.sh/)

* [NASM Docs](https://nasm.us/docs.php)

* http://www.posix.nl/linuxassembly/nasmdochtml/nasmdoca.html

* [Intel® 64 and IA-32 Architectures Software Developer’s Manual](https://software.intel.com/en-us/download/intel-64-and-ia-32-architectures-sdm-combined-volumes-1-2a-2b-2c-2d-3a-3b-3c-3d-and-4)
