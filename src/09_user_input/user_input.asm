%include "functions.asm"

SECTION .data
  msg1 db "Please enter your name: ", 0x00
  msg2 db "Hello, ", 0x00

SECTION .bss
  ; var_byte1:  resb 1    ; reserve space for 1 byte
  ; var_word1:  resw 1    ; reserve space for 1 word
  ; var_dword1: resd 1    ; reserve space for 1 double word
  ; var_qword1: resq 1    ; reserve space for 1 double precision float (quad word)
  ; var_float1: rest 1    ; reserve space for 1 extended precision float
  uinput:     resb 255  ; reserve a 255 byte space in memory for the users input string

SECTION .text
global _start

_start:
  mov eax, msg1
  call str_print_LF

  mov edx, 255      ; number of bytes to read
  mov ecx, uinput   ; reserved space to store our input (known as buffer)
  mov ebx, 0        ; write to the STDIN file
  mov eax, 3        ; invoke sys_read (kernel OPCODE 3)
  int 0x80

  mov eax, msg2
  call str_print

  mov eax, uinput   ; move our buffer into eax (Note: input contains a linefeed)
  call str_print

  call sys_exit_ok
