%include "functions.asm"

SECTION .text
global _start

_start:
  mov ecx, 1        ; initialise ecx



; print ascii numbers from 1 to 10
next_count:
  cmp ecx, 11       ; compare ecx to 11
  jae finished      ; jump if above or equal

  mov eax, ecx      ; move ecx into eax
  add eax, 48       ; convert the loop index to an ascii number by adding 48
  push eax          ; push eax onto the stack
  mov eax, esp      ; move the address of the top element in the stack into eax
  call str_print_LF
  
  pop eax
  inc ecx
  
  jmp next_count



finished:
  call sys_exit_ok
