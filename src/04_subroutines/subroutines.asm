SECTION .data
  msg db "Hello, brave new world!", 0x0A

SECTION .text
global _start

_start:
  mov eax, msg  ; move the address of msg into EAX
  call strlen   ; call the function to calculate the length of the msg

  mov edx, eax ; our function leaves the result in EAX
  mov ecx, msg
  mov ebx, 1
  mov eax, 4
  int 0x80

  mov ebx, 0
  mov eax, 1
  int 0x80

strlen:         ; function declaration
  push ebx      ; push the value in EBX onto the stack to preserve it while we use EBX in this function
  mov ebx, eax  ; move the address in EAX into EBX (Both point to the same segment in memory)
 
nextchar:
  cmp byte[eax], 0
  jz finished
  inc eax
  jmp nextchar

finished:
  sub eax, ebx
  pop ebx       ; pop the value on the stack back into EBX
  ret           ; return to where the function was called