section .data
msg     db "Hello, World!", 0x0A; assign msg variable with your message string

section .text
global  _start

_start:
	mov edx, 13; 13 bytes of strings
	mov ecx, msg; move the memory address of our message string into ecx
	mov ebx, 1; write to the STDOUT file
	mov eax, 4; kernel OPCODE 4 for invoking sys_write
	int 0x80
