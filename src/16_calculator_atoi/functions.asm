
;------------------------------------------------------------------------
; int atoi(string number)
;
; Convert the ASCII string pointed to by eax
; into an integer and store the result in eax.
atoi:
    push ebx
    push ecx
    push edx
    
    ; get length of string and store it in ecx
    push eax
    call str_len
    mov ecx, eax
    pop eax

    ; set ebx to 0x00 because this is the register in
    ; which we temporarily store the integer
    xor ebx, ebx
    
    ; set edx to 0x00, because dh acts as the stop byte for strings
    xor edx, edx
    
  atoi_next_char:
    cmp ecx, 0x00
    jz atoi_finished
   
    mov dl, byte[eax]

    ; Check if ASCII character is a number,
    ; if not stop the conversion and return 0x00
    call atoi_check_is_number
    cmp edx, 0x00
    jz atoi_finished

    
    ; Add the next number ASCII character,
    ; stored in edx to ebx.
    ; The digit position of the number is
    ; the current value of ecx.
    push eax
        
    push ebx    
    mov ebx, 0x0A
    call pow
    pop ebx

    ; Convert ASCII character to number    
    sub edx, 0x30
            
    ; Multiply eax by edx
    mul edx
    add ebx, eax
    
    pop eax
    
    
    inc eax
    dec ecx
    jmp atoi_next_char
  
  ; Checks if the ASCII character stored in edx is
  ; between 0x30 and 0x39, if not set edx and 
  ; ebx to 0x00.
  atoi_check_is_number:
    cmp edx, 0x30 ; ASCII '0'
    jl atoi_check_is_number_reset
    
    cmp edx, 0x39 ; ASCII '9'
    jle atoi_check_is_number_finished
 
    atoi_check_is_number_reset:
      xor edx, edx
      xor ebx, ebx
    
    atoi_check_is_number_finished:
    ret
  
  atoi_finished:
    mov eax, ebx
    
    ; Divide eax by 0x0A
    mov ebx, 0x0A
    div ebx
    
    pop edx
    pop ecx
    pop ebx
  
  ret



;------------------------------------------------------------------------
; int pow(int x, int y)
; 
; Returns the y power of x, the result is stored in eax.
; x has to be stored in ebx and y has to be stored in ecx.
; 
; Only supports unsigned integers.
pow:
    ; in case ecx is 0x00 return 0x01 immediately
    ; x^0x00 = 0x01
    cmp ecx, 0x00
    jnz pow_initialize
    mov eax, 0x01  

  ret
  
  pow_initialize:  
    ; We push edx onto the stack as well, because the
    ; result of mul is stored in EDX:EAX if the operandsize
    ; is 32.
    push ecx
    push edx 

    ; Set the result register to base (ebx)
    mov eax, ebx
    
    ; Decrement exponent (ecx) by 0x01
    dec ecx
        
  pow_next:
    cmp ecx, 0x00
    jz pow_finished
    
    ; Multiply eax by ebx    
    mul ebx

    dec ecx
    jmp pow_next
    
  pow_finished:          
    pop edx
    pop ecx
    
  ret



;------------------------------------------------------------------------
; void print_LF(void)
;
; Print a single linefeed character (0x0A) to STDOUT.
; 0x0A is the ASCII character for a linefeed
print_LF:
  ; push eax onto the stack to preserve it while we
  ; use the eax register in this function
  push eax        
  mov  eax, 0x0A 

  ; push the linefeed onto the stack so we can get the address
  push eax        
  ; move the address of the current stack pointer into eax for sprint
  mov  eax, esp   
  call print_str  
  
  ; remove our linefeed character from the stack
  pop eax         
  ; restore the original value of eax before our function was called
  pop eax         
  
  ret



;------------------------------------------------------------------------
; void print_str(string message)
;
; Print the string pointed to by eax to stdout.
print_str:

  ; push the values of all registers onto the stack
  push edx
  push ebx
  push ecx

  ; move the address of the message into ecx before calling str_len
  mov ecx, eax, 
  
  call str_len

  ; move the length of the string into edx and pop the previous value in eax from the stack
  mov edx, eax
    
  mov ebx, 0x01
  mov eax, 0x04
  int 0x80

  ; restore all values from the stack
  pop ecx
  pop ebx
  pop edx
  
  ret



;------------------------------------------------------------------------
; void print_int(int number)
;
; Print the number stored in eax to stdout.
print_int:
    ; push the initial value of the registers onto the stack
    push ebx
    push ecx 
    push edx   
    push eax
    push esi
      
    mov esi, eax
    mov ecx, 0x00
    mov ebx, 0x0A

  ; get the amount of digits of the number stored in ecx
  print_int_get_length:
    xor edx, edx
    div ebx        ; divide the value stored in eax by ebx

    inc ecx
    cmp eax, 0x00
    jne print_int_get_length
    
    ; ecx is now equal to the amount of digits

    mov eax, esi    ; eax is the dividend
    mov esi, ecx

  print_int_push_all_digits:
    xor edx, edx; set the remainder of the division to 0

    div ebx         ; divide the value stored in eax by 0x0A
    or edx, 0x30    ; convert the remainder to the corresponding ascii character

    ; print the ascii character in edx
    push edx

    dec ecx
    cmp ecx, 0x00
    jg print_int_push_all_digits

    mov ecx, esi

  print_int_pop_all_digits:
    mov eax, esp
    call print_str

    pop edx

    dec ecx
    cmp ecx, 0x00
    jg print_int_pop_all_digits

  print_int_finished:
    ; pop the initial value of the registers from the stack
    pop esi
    pop eax
    pop edx
    pop ecx
    pop ebx

  ret



;------------------------------------------------------------------------
; int str_len(string message) 
;
; Calculate the length of the string pointed
; to by eax and store the length in eax.
str_len:
  push ebx
  mov  ebx, eax

  str_len_nextchar:
    cmp byte[eax], 0x00
    jz  str_len_finished
    inc eax
    jmp str_len_nextchar

  str_len_finished:
    sub eax, ebx
    pop ebx

  ret



;------------------------------------------------------------------------
; void sys_exit(int code)
;
; Exit the program with the exit code stored in ebx
; and restore resources.
sys_exit:
  mov eax, 1
  int 0x80
  ret



;------------------------------------------------------------------------
; void sys_exit_ok(void)
;
; Exit the program with the exit code 0x00 restore resources.
sys_exit_ok:
  xor ebx, ebx
  mov eax, 1
  int 0x80
  ret