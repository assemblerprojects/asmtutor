%include "functions.asm"

SECTION .data
  msg1 db "No more args. Result: ", 0x00
  msg2 db "Number of args: ", 0x00

SECTION .text
global _start

_start:
    pop ecx ; number of args

    ; First arg is path of the executable.
    ; We skip the first argument
    pop eax
    dec ecx 

    mov eax, msg2
    call print_str

    ; print number of args
    mov eax, ecx
    call print_int
    
    call print_LF

    ; set edx to 0x00
    xor edx, edx

  next_arg:
    cmp ecx, 0x00
    jz finished

    ; process args

    ; eax now contains address of arg as string
    pop eax
    call atoi
    add edx, eax
        
    dec ecx
    jmp next_arg

  finished:
    mov eax, msg1
    call print_str

    mov eax, edx
    call print_int
    
    call print_LF

  call sys_exit_ok
