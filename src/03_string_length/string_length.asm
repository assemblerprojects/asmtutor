SECTION .data
  msg db "Hello, brave new world!", 0x0A ; we can modify this now without having to update anywhere else in the program

SECTION .text
global _start

_start:
  mov ebx, msg      ; move the address of msg string into EBX
  mov eax, ebx      ; move the address in EBX into EAX as well (both registers pointing to the same address)

nextchar:
  cmp byte[eax], 0  ; compare the byte pointed to by the current address of EAX against 0 (Zero is an end of string delimiter)
  jz  finished      ; if the ZF (Zero Flag) has been set, jump to the label "finished"
  inc eax           ; increment the address in EAX by one byte
  jmp nextchar      ; jumo to the label "nextchar"

finished:
  sub eax, ebx      ; subtract the address in EBX from the address in EAX
                    ; remember both registers started pointing to the same address (see line 9)
                    ; but EAX has been incremented one byte for each character in the message string
                    ; when you subtract one memory address from another of the same type
                    ; the result is number of segments between them - in this case the number of bytes 
  
  mov edx, eax      ; EAX is now equal to the number of bytes in msg
  mov ecx, msg      
  mov ebx, 1
  mov eax, 4
  int 0x80

  mov ebx, 0
  mov eax, 1
  int 0x80