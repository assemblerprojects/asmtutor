; run with: ./passing_arguments "This is one argument" "This is another" 101

%include "functions.asm"

SECTION .text
global _start

_start:
  pop ecx           ; first value on the stack is the number of args

next_arg:
  cmp ecx, 0x00     ; check if there are any arguments left
  jz no_more_args   ; if ZF is set, jump to label no_more_args

  pop eax           ; pop the next arg off stack
  call str_print_LF ; print the argument

  dec ecx           ; decrease ecx, number of arguments left by 1
  jmp next_arg

no_more_args:
  call sys_exit_ok