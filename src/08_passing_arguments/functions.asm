;------------------------------------------------------------------------
; int str_len(string message) 
;
; Calculate the length of a the string pointed
; to by eax and store the resulting length in eax.
str_len:
  push ebx
  mov  ebx, eax

nextchar:
  cmp byte[eax], 0
  jz  finished
  inc eax
  jmp nextchar

finished:
  sub eax, ebx
  pop ebx
  ret



;------------------------------------------------------------------------
; void str_print(string message)
;
; Print the string pointed to by eax to stdout.
str_print:

  ; push the values of all registers onto the stack
  push edx
  push ecx
  push ebx
  push eax

  call str_len

  ; move the length of the string into edx and pop the previous value in eax from the stack
  mov edx, eax
  pop eax

  mov ecx, eax, ; move the address of the message into ecx
  mov ebx, 1
  mov eax, 4
  int 0x80

  ; restore all values from the stack
  pop edx
  pop ecx
  pop ebx
  
  ret



;------------------------------------------------------------------------
; void str_print_LF(string message)
;
; Print the string pointed to by eax to stdout with a linefeed character (0x0A).
; 0x0A is the ASCII character for a linefeed
str_print_LF:
  ; print the message
  call str_print

  ; print the linefeed character
  push eax        ; push eax onto the stack to preserve it while we use the eax register in this function
  mov  eax, 0x0A   ; move 0x0A into 
  push eax        ; push the linefeed onto the stack so we can get the address
  mov  eax, esp    ; move the address of the current stack pointer into eax for sprint
  
  call str_print  ; print the linefeed character

  pop eax         ; remove our linefeed character from the stack
  pop eax         ; restore the original value of eax before our function was called
  
  ret



;------------------------------------------------------------------------
; void sys_exit()
;
; Exit the program with the exit code stored in ebx and restore resources.
sys_exit:
  mov eax, 1
  int 0x80
  ret



;------------------------------------------------------------------------
; void sys_exit_ok()
;
; Exit the program with the exit code 0 restore resources.
sys_exit_ok:
  mov ebx, 0
  mov eax, 1
  int 0x80
  ret
