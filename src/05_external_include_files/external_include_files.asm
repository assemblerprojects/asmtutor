%include "functions.asm"

	SECTION .data
	msg1    db "Hello, brave new world!", 0x0A
	msg2    db "This is how we recycle in NASM.", 0x0A

	SECTION .text
	global  _start

_start:
	mov  eax, msg1
	call str_print

	mov  eax, msg2
	call str_print

	call sys_exit_ok
