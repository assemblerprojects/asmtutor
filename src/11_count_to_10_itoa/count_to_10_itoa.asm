%include "functions.asm"

SECTION .data
  msg1 db "Programm end.", 0x00
  msg2 db "Programm start.", 0x00

SECTION .text
global _start

_start:
  mov eax, msg2
  call print_str
  call print_LF
  
  mov edx, 101
  mov ecx, 102
  mov ebx, 103
  mov esi, 104
  
  mov eax, 514
  call print_int
  call print_LF

  mov eax, msg1
  call print_str
  call print_LF

  call sys_exit_ok
