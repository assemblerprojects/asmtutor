;------------------------------------------------------------------------
; void print_LF()
;
; Print a single linefeed character (0x0A) to STDOUT.
; 0x0A is the ASCII character for a linefeed
print_LF:
  push eax        ; push eax onto the stack to preserve it while we use the eax register in this function
  mov  eax, 0x0A  ; move 0x0A into 
  push eax        ; push the linefeed onto the stack so we can get the address
  mov  eax, esp   ; move the address of the current stack pointer into eax for sprint
  
  call print_str  ; print the linefeed character

  pop eax         ; remove our linefeed character from the stack
  pop eax         ; restore the original value of eax before our function was called
  
  ret



;------------------------------------------------------------------------
; void print_str(string message)
;
; Print the string pointed to by eax to stdout.
print_str:

  ; push the values of all registers onto the stack
  push edx
  push ebx
  push ecx

  ; move the address of the message into ecx before calling str_len
  mov ecx, eax, 
  
  call str_len

  ; move the length of the string into edx and pop the previous value in eax from the stack
  mov edx, eax
    
  mov ebx, 1
  mov eax, 4
  int 0x80

  ; restore all values from the stack
  pop ecx
  pop ebx
  pop edx
  
  ret



;------------------------------------------------------------------------
; void print_int(int number)
;
; Print the number stored in eax to stdout.
print_int:
  ; push the initial value of the registers onto the stack
  push ebx
  push ecx 
  push edx   
  push eax
  push esi
    
  mov esi, eax
  mov ecx, 0
  mov ebx, 0x0A

  ; get the amount of digits of the number stored in ecx
  print_int_get_length:
    mov edx, 0  
    div ebx        ; divide the value stored in eax by ebx

    inc ecx
    cmp eax, 0
    jne print_int_get_length
    
    ; ecx is now equal to the amount of digits

    mov eax, esi    ; eax is the dividend
    mov esi, ecx

  print_int_push_all_digits:
    mov edx, 0      ; set the remainder of the division to 0

    div ebx         ; divide the value stored in eax by 0x0A
    or edx, 0x30    ; convert the remainder to the corresponding ascii character

    ; print the ascii character in edx
    push edx

    dec ecx
    cmp ecx, 0
    jg print_int_push_all_digits

    mov ecx, esi

  print_int_pop_all_digits:
    mov eax, esp
    call print_str

    pop edx

    dec ecx
    cmp ecx, 0
    jg print_int_pop_all_digits

  print_int_finished:
    ; pop the initial value of the registers from the stack
    pop esi
    pop eax
    pop edx
    pop ecx
    pop ebx

  ret



;------------------------------------------------------------------------
; int str_len(string message) 
;
; Calculate the length of a the string pointed
; to by eax and store the resulting length in eax.
str_len:
  push ebx
  mov  ebx, eax

  str_len_nextchar:
    cmp byte[eax], 0
    jz  str_len_finished
    inc eax
    jmp str_len_nextchar

  str_len_finished:
    sub eax, ebx
    pop ebx

  ret



;------------------------------------------------------------------------
; void sys_exit()
;
; Exit the program with the exit code stored in ebx and restore resources.
sys_exit:
  mov eax, 1
  int 0x80
  ret



;------------------------------------------------------------------------
; void sys_exit_ok()
;
; Exit the program with the exit code 0 restore resources.
sys_exit_ok:
  mov ebx, 0
  mov eax, 1
  int 0x80
  ret



