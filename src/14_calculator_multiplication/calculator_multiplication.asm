%include "functions.asm"

SECTION .text
global _start

_start:
  mov eax, 90
  mov ebx, 10
  mul ebx
  call print_int
  call print_LF

  call sys_exit_ok