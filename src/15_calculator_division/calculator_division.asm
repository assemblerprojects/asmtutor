%include "functions.asm"

SECTION .data
  msg_result db "Result is ", 0x00
  msg_remainder db "Remainder is ", 0x00

SECTION .text
global _start

_start:
  mov eax, 90
  mov ebx, 11
  div ebx      ; divides eax by ebx. The result is stored in eax and the remainder in edx

  ; print result
  push eax
  mov eax, msg_result
  call print_str
  pop eax
  
  call print_int
  call print_LF

  ; print remainder
  mov eax, msg_remainder
  call print_str

  mov eax, edx
  
  call print_int
  call print_LF

  call sys_exit_ok