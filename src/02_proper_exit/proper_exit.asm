section .data
msg     db "Hello, World!", 0x0A

section .text
global  _start

_start:
	;   Print msg
	mov edx, 13
	mov ecx, msg
	mov ebx, 1
	mov eax, 4
	int 0x80

	;   call sys_exit
	mov ebx, 0; exitcode of this program, 0 for no errors
	mov eax, 1; kernel OPCODE for sys_exit
	int 0x80; request interrupt on libc
